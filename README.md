# Referência 

[Learning Spark: Lightning-Fast Data Analytics](https://www.amazon.com.br/Learning-Spark-2e-Jules-Damji/dp/1492050040/ref=sr_1_7?__mk_pt_BR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=25980MX842H5S&keywords=learning+pyspark&qid=1645971242&sprefix=learning+pyspark%2Caps%2C175&sr=8-7&ufe=app_do%3Aamzn1.fos.25548f35-0de7-44b3-b28e-0f56f3f96147)

# Introdução
Para falar de spark sempre temos que falar primeiro de Hadoop MapReduce. Porém esses dois sistemas operam de formas distintas, dando ao spark maior velocidade que o MR.

Ambos trabalham com sistemas de clusters e processamento distribuido. 

Porém no MR os workers recebem o codigo que precisam rodar em cima dos seus dados efetuando as operação necessários e enviando os resustados para a função de reduce, que irá escrever os dados em um sistema de armazenamento distribuido que poderá ser acessado pelas aplicações. E também a cada operação intermediária o sistema de MR escreve os resultados no disco local, fazendo com que a performance seja definida pela performance de I/O do disco.

Já o spark efetua os processos intermediários in-memory storage, fazendo com que ele seja muito mais rápido que o Hadoop MapReduce.

O spark foi desenhado com quatro características chaves:

- Speed
- Ease of use
- Modularity
- Extensibility

## Speed
O spark utiliza processamento multithreading e paralelo eficientes. Para fazer isso ele constroi grafos de processamento - Directed acyclic graph (DAG) - que são construidos de forma a construir um fluxo computacional eficiente que será decomposto em tarefas que serão distribuidas entre os workers do cluster. E todos os resultados intermediários são retidos na memoria, somente indo ao disco se e quando necessário, melhorando assim a performance.

## Ease of Use
O spark prove uma abstração simples das estruturas de dados que é chamada de Resilient Distributed Dataset (RDD) e em cima dessa abstração vêm outras abstrações de nível superior, como DataFrames e Datasets.

## Modularity
O spark trabalha com sistema de APIs, que podem ser chamadas por qualquer linguagem (dentre as suportadas) e por baixo tudo irá ser convertido para o mesmo bytecode que irá rodar na JVM. Essas APIs são Spark SQL, Spark Structured Streaming, Spark MLlib e GraphX.

## Extensibility
O spark desacoplou as partes computacional e de armazenamento, diferentemente do Apache Hadoop. Ou seja, o spark não precisa que os dados estejam junto a ele, ele consegue ler os dados diversas fontes diferentes, como por exemplo o Apache Cassandra, Apache HBase, mongoDB, S3, Apache Kafka, Kinesis.

O spark também possibilita que os desenvolvedores façam outros conectores que não são suportados oficialmente, e eles podem ser vistos [aqui](https://spark.apache.org/third-party-projects.html)

# Ambiente unificado
O spark tem o objetivo de ser um ambiente unificado para processamento de big data. Para isso ele faz uso das suas APIs.

Nós usamos as APIs para escrever os códigos spark em qualquer linguagem suportada, como Java, R, Scala ou Python (oficiais), que serão convertido em DAGs que serão distribuidos entre os workes e executados pela JVM (como isso será convertido em bytecode, não importa a linguagem que escreva o código, ele terá sempre a mesma performance)

## Spark SQL
Esse modulo funciona com dados estruturados. Que podem vir de tabelas em em sistemas de bancos relacionais, ou em arquivos em formato estruturado (CSV, Json, Avro, Parquet e etc.), e então com esses dados, construir tabelas no spark de permanente ou temporária.

E você poderá executar queries SQL-like ([ANSI SQL:2003-compliant](https://en.wikipedia.org/wiki/SQL:2003)) em cima dessas tabelas.

Um exemplo disso seria ler um arquivo json, que está no S3, criar uma tabela temporária desses dados e então executar uma query SQL em cima dessa tabela que estará na memória.

## Spark MLlib
Esse módulo prove muitos algoritmos populares de machine learning construidos em cima da High-Level APIs de DataFrame.

Essas APIs permitem construir pipelines, criar modelos permanentes para deploy. Também permite efetuar operaçoes de algebra linear e estastística. E esse módulo também inclui outra api de low-level ML primitives, que inclui um algoritmo genérico de gradient descent optimization.

## Spark Structured Streaming
necessário para desenvolvimentos de big data em cenários de tempo real, tanto para dados estáticos quanto para streaming vindos de engines como o Apache Kafka. Esse stream de dados será visto como uma tabela que crescerá continuamente com os dados sendo adicionados ao fim dela. Os desenvolvedores poderão tratar essa tabela e efetuar queries contra ela exatamente igual a uma tabela estática.

## GraphX
Utilizado para manipulação de dados salvos em estrutura de grafos (redes sociais, pontos de rota - de rede ou até mesmo de transito) e efetuar operações em paralelo.

## Excução distribuida do apache spark
O spark é uma engine de processamento de dados distribuida, sendo que seus componentes trabalham de forma colaborativa nos clusters de máquinas.

Em alto nível, a aquitetura do spark consiste em um driver que é responsável por orquestrar as operações em paralelo no cluster. O driver acessa os componentes do cluster, ou seja, os spark executors e cluster manager, via SparkSession.

### Spark driver
Responsável por instanciar a SparkSession, sendo que suas responsabilidades são:

- Comunicar com o cluster manager;
- Requisitar recursos (CPU, memoria, etc..) do cluster manager para os Spark executors (JVM);
- Transformar todas as operações em DAG, agendar as execuções e distirbui-las entre os executores;
- Uma vez que os recursos estão alocados, o driver passa a se comunicar diretamente com os executores.

### SparkSession
O SparkSession prove um unico ponto de entrada para todas as funcionalidades do spark, sendo possível criar uma para cada JVM e utilizá-las para executar operações.

### Cluster manager
Gerenciar e alocar recursos para o cluster de nós em que a aplicação spark irá executar. Sendo que o Spark suporta Apache Hadoop YARN, Apache Mesos e Kubernetes.

<img src="./images/fig_01.svg"/>

## Modos de Deploy
O spark suporta uma variedade de modos de deploy, possibilitando diferentes configurações e ambientes. Isso se deve ao fato de que o cluster manager é agnostico e pode rodar cm qualquer ambiente desde que ele consiga gerenciar os executores e requisitar recuros. Os sistemas mais populares hoje são Apache Hadoop YARN e Kubernetes

|Mode|Spark Driver|Spark Executor|Cluster Manager|
|-|-|-|-|
|Local|Roda em uma única JVM, single node (PC pessoal)|Roda na mesma JVM que o driver|Roda no mesmo host|
|Standalone|Pode rodar em qualquer node do cluster|Cada node no cluster terá sua propria JVM para executar as tarefas|É alocado arbitrariamente em qualquer um dos hosts do cluster|
|YARN (client)|Roda em um client, que não faz parte do cluster|YARN's NodeManager's container|YARN's Resouce Manager roda com o YARN's application Master para alocar containers nos NdeManagers para os executores|
|YARN (cluster)|Roda com o YARN application Master|Igual ao YARN client mode|Igual ao YARN client mode|
|Kubernetes|Roda em um kubernetes pod|Cada worker roda em seu proprio pod|Kubernetes Master|

## Distribuição de dados e partições
Os dados são distribuidos no sistema de arquivos como partições. Partições permitem maior eficiencia em processamento paralelo. O spark permite que os dados sejam quebrados em chuncks ou partições para que sejam distribuidos para processamento nos executores, sendo que cada executor irá utilizar os dados mais proximos a eles, assim minimizando a banda de internet.


# Download Spark
Vou utilizar python, então basta ter o java instalado na máquina e o python (com o pip) e então execute 

```sh
pip install pyspark
```

> OBS.: Para comparar se a versão do java e tudo mais está correta na sua máquina, vou deixar aqui o resultado do comando `java --version`
```sh
java --version

>>> output
    openjdk version "1.8.0_312"
    OpenJDK Runtime Environment (build 1.8.0_312-8u312-b07-0ubuntu1~20.04-b07)
    OpenJDK 64-Bit Server VM (build 25.312-b07, mixed mode)
```

Agora se quiser usar outra linguage, recomendo dar uma olhada na pagina de [download do spark](https://spark.apache.org/downloads.html)

# Getting Started

## Spark Shell
O spark shell, assim como python shell, é utilizado para ad hoc data analysis, ou seja, para exerimentar coisas, testes de código, conhecer funções, mas é um otimo lugar para começar a utilizar a ferramenta. Para isso basta digitar o comando:

```sh
pyspark
```

> OBS.: Como estamos utilizando a versão para python, esse shell do spark vai executar qualquer comando python.

O pyspark shell já vai vir com uma spark session que foi atribuida a variável chamada spark, então você pode começar testando ele digitando o comando

```py
>>> spark.version
```

## Utilizando na máquina local
No exemplo abaixo vamos apenas puxar esse arquivo de README.md para o spark no fomrmato de dataframe e contar quantas linhas existem atualmente.

```py
>>> strings = spark.read.text('README.md')
>>> strings.show(10, truncate=False)
>>> strings.count()
``` 

## Entendendo os conceitos de uma aplicação spark

Vamos começar definindo alguns termos

- Application:
    - Um programa construido em spark utilizando suas APIs. Ele consiste do uso do driver e seus executores no cluster

- SparkSession
    - Um objeto que é um ponto de entrada para interagir com as funcionalidades do spark e suas APIs.

- Job:
    - Consistem em multiplas tasks que serão realizadas paralelamente.

- Stage
    - Cada job é dividia em pequenos sets de tasks que são chamados de stages, que dependem um do outro

- Tasks
    - Uma unica unidade de trabalho que será executada pelo Spark Executor.

> OBS.: O coração de toda aplicação spark é o spark driver, que cria o objeto SparkSession. E sempre que se trabalha com o SparkShell, o driver já faz parte do shell e a SparkSession já é criada automaticamente e acessada por meio do objeto `spark`


### Spark Jobs
O driver converte sua aplicação Spark em uma ou mais Spark Jobs. E então transforma cada job em uma DAG. Esse, em essência, é o plano de execução do Spark, em que cada nó com uma DAG pode ser um ou multiplos Spark Stages.

### Spark Stages
Os Stages são criados com base em quais operações podem ser feitas de maneira serial ou paralela. Nem todas as Spark operations podem acontecer em um unico estágio, então eles podem ser divididos em múltiplos estágios.

### Spark Tasks
Cada stage é divido em Spark Tasks (uma única unidade de execução), que será encaminhada a cada um dos Spark Executors; cada task é mapeada para um unico core e trabalha em uma única partição de dados.

<img src="./images/fig_02.svg" />


### Transformations, Actions e Lazy Evaluation
As operações que o spark realiza podem serem classificadas em dois tipos, `transformations` e `actions`. Transformations, assim como o nome sugere, transforma o Spark DataFrame em um novo DataFrame sem modificar o original, dando-o a propriedade da imutabilidade. Em outras palavras, operações como `select()` ou `filter()` não modificam o DataFrame original, elas retornam um novo com os resultados.
Todas as transformações são lazy evaluated. Ou seja, os resultados não são computados imediatamente, porém o Spark guarda cada transformação na forma de "linha temporal de ações" a qual ele utiliza para formular seu plano de execução de forma otimizada e eficiente, sendo que ele só vai computar algo quando os dados tem que serem "tocados", ou seja, quando uma ação é disparada, como por exemplo a visualização dos dados. Em outras palavras, o Spark acumula as operações de transformações para otimizá-las e só computa quando realmente necessário.

Alguns exemplos de transformações e ações no spark são as funções

|Transformations|Actions|
|-|-|
|orderBy()|show()   |
|groupBy()|take()   |
|filter() |count()  |
|select() |collect()|
|join()   |save()   |

Abaixo segue um exemplo em que o código lê o arquivo README.md e conta quantas vezes a palavra `Spark` apareceu, sendo que ele possui duas operações de transformação (read e fielter) e apenas uma de action (count), logo nada acontece até que a actions seja disparada

```py
strings = spark.read.text('README.md')
filtered = strings.filter(strings.value.contains('Spark'))
filtered.count()
```

### Narrow and Wide Transformations
Todas as transformações que o spark realiza podem ser classificadas de duas formas:
- Narrow transformation:
    - São transformações que com uma única partição de dados consegue produzir uma unica partição de dados. Ou seja para a operação ocorrer não existe dependência entre os dados das partições, um exemplo seriam as operações de filter e contains, que não precisam conhecer todo o conjunto de dados para serem realizadas
- Wide transformation:
    - São transformações que precisam verificar o conjuto de dados como um todo (varias partições), exemplos de operações são groupBy e orderBy, que são impossíveis de acontecerem sem verificar todo o conjunto de dados.


### Exemplo 01 - Contando M&Ms
Vou escrever uma aplicação spark que irá obter a quantidade por cor de M&Ms. Para isso crie um arquivo chamado `main.py`, copie o código abaixo para esse arquivo, e também copie o dataset [mnm_dataset](./data/mnm_dataset.csv). Para executá-lo basta rodar o comando

```sh
python main.py data/mnm_dataset.csv
```

```py
import sys

from pyspark.sql import SparkSession
from pyspark.sql.functions import count

# criar a SparkSession
spark = SparkSession.builder.appName('MnMCount').getOrCreate()

# pegar nome do arquivo que foi passado pelo parâmetro
mnm_file = sys.argv[1]

# lendo o arquivo csv
mnm_df = (
    spark.read.format('csv')
    .option('header', 'true')
    .option('inferSchema', 'true')
    .load(mnm_file)
)

# abaixo temos o spark 
#     - selecionar as colunas
#     - agrupar por State e Color e agregar o coluna Count com a função count e dando o nome de Total para ela
#     - e ordenar por essa coluna Total
count_mnm_df = (
    mnm_df.select('State', 'Color', 'Count')
    .groupBy('State', 'Color')
    .agg(count('Count').alias('Total'))
    .orderBy('Total', ascending=False)
)

# mostrar os primeiros 60
count_mnm_df.show(n=60, truncate=False)

# contar numero de linhas
print(f"Total 'Rows' = {count_mnm_df.count()}")

# faz o mesmo processo de antes, porém só com M&Ms do estádo CA
ca_count_mnm_df = (
    mnm_df.select('State', 'Color', 'Count')
    .where(mnm_df.State == 'CA')
    .groupBy('State', 'Color')
    .agg(count('Count').alias('Total'))
    .orderBy('Total', ascending=False)
)

# mostar os 10 primeiros
ca_count_mnm_df.show(n=10, truncate=False)

# parar a sessão spark
spark.stop()
```

# Apache Spark's Structured APIs

## RDD

RDD é a abstação mais básica no spark para alocar os dados. Ela possui três caractristicas principais.

- Dependencia:
    - Uma lista de dependênias instrui o spark como uma RDD deve ser construida, assim, apartir dessa lista é sempre possível reconstruir a RDD, dando resiliência ao spark.
- Partições:
    - A capacidade de particionar os dados permite que o spark paralelize as operações pelos executors.
- Computa funções: Partição => Iterator[T]
    - RDDs possuem uma função que irá computar os dados de uma partição para uma dada função de iteração.

Essas caracteristicas possúem suas vantagens, porém possúem desvantagens muito relevantes:
    - Não importa a operação que irá efetuar, join, filter, select ou aggregation, o spark sempre irá ver expressões lambda. 
    - Os tipos de dados para o Iterator[T] são opacos para RDDs, o Spark encherga apenas objetos python genéricos para entrar nessa função. Isso atrapalha o spark a rearranjar os calculos de forma eficiente no plano de execução.

Para resolver esse problema, na versão 2.x do spark algumas modificações foram feitas. Uma dessas mudanças foi incluir exprossões comuns ao mundo de analise de dados como high-level operations, como filtering, selecting, counting, aggregating, averaging e grouping. Isso deu ao spark uma performance muito melhor, e também maior expressividade e facilidade de se utilizar, veja o comparativo entre os dois exemplos, um utilizando RDD e outro DataFrame (que vem com essas melhorias - já falaremos mais deles), no exemplo abaixo, queremos agregar todas as idades por cada nome e então ver a média das idades.

- `Com RDD`
```py
# Cria o RDD com os dados
dataRDD = sc.parallelize(
    [('Brooke', 20), ('Denny', 31), ('Jules', 30), ('TD', 35), ('Brooke', 25)]
)

# utiliza o map para iterar na rdd efetuando a operação de reduceByKey para agrupar por nome e depois fazer a média
agesRDD = (
    dataRDD.map(lambda x: (x[0], (x[1], 1)))
    .reduceByKey(lambda x, y: (x[0] + y[0], x[1] + y[1]))
    .map(lambda x: (x[0], x[1][0] / x[1][1]))
)
```

- `Com DataFrame`
```py
from pyspark.sql import SparkSession
from pyspark.sql.functions import avg

# Cria a sparkSession
spark = SparkSession.builder.appName('AuthorsAges').getOrCreate()

# Cria o dataframe
data_df = spark.createDataFrame(
    [('Brooke', 20), ('Denny', 31), ('Jules', 30), ('TD', 35), ('Brooke', 25)],
    ['name', 'age'],
)

# Agrupa por nome e agrega a idade já efetuando a média
avg_df = data_df.groupBy('name').agg(avg('age'))

# Mostra o resultado
avg_df.show()
```

Veja que existe uma diferença gigante entre os códigos, o primeiro estamos dizendo ao spark `COMO FAZER` as operações, já no segundo estamos dizendo `O QUE FAZER` isso gera uma melhora muito grande, pois uma vez que seguimos para a abordagem de dizer ao spark o que fazer, ele já tem pré definido como fazer aquilo da melhor forma possível.

E também ganhamos em legibilidade e reutilização do código, pois o código com RDDs é complexo de ler e entender, já o código com DataFrames é simples e descritivo, e como são APIs de high-level o código em python, scala, java... serão quase identicos, já o código com RDDs será completamente diferente em cada uma dessas linguagens.

## DataFrame
Os DataFrames do Spark foram inspirados nos DataFrames do pandas, em estutura, formato, e nos operações. Os DataFrames do spark são distribuidos na memoria em tabelas com colunas nomeadas e com schemas, ou seja, cada coluna possui um tipo de dados já definido (integer, string, array, map, real, date, timestamp, etc). isso torna o dataframe facilmente interpretado aos humanos, pois visivelmente é como uma tabela.

Quando os dados são visualizados de forma estruturada como tabela, não é só mais fácil de ver com os olhos, mas também mais fácil de trabalhar, pois agora fica mais simples de operar em colunas e linhas.

DataFrames também são imutáveis no Spark, ou seja, você pode aplicar transformações a ele, adicionando ou mudando nome ou tipos de dados de colunas que um novo dataframe será criado e o antigo será preservado.

- Tipos de dados básicos do Spark

|Data type|Value assigned in Python|API to instantiate|
|-|-|-|
|ByteType   |int            |DataTypes.ByteType   |
|ShortType  |int            |DataTypes.ShortType  |
|IntegerType|int            |DataTypes.IntegerType|
|LongType   |int            |DataTypes.LongType   |
|FloatType  |float          |DataTypes.FloatType  |
|DoubleType |float          |DataTypes.DoubleType |
|StringType |str            |DataTypes.StringType |
|BooleanType|bool           |DataTypes.BooleanType|
|DecimalType|decimal.Decimal|DecimalType          |

Veja a referência da [documentação](https://spark.apache.org/docs/latest/sql-ref-datatypes.html)

> OBS.: Em python, todos esses tipos estão em `from pyspark.sql.types import *` é só digitar o tipo que você verá `ByteType()`

- Tipos complexos

|Data type|Value assigned in Python|API to instantiate|
|-|-|-|
|BinaryType   |bytearray                                           |BinaryType()                           |
|TimestampType|datetime.datetime                                   |TimestampType()                        |
|DateType     |datetime.date                                       |DateType()                             |
|ArrayType    |List, tuple, or array                               |ArrayType(dataType, [nullable])        |
|MapType      |dict                                                |MapType(keyType, valueType, [nullable])|
|StructType   |List or tuple                                       |StructType([fields])                   |
|StructField  |A value type corresponding to the type of this field|StructField(name, dataType, [nullable])|

### Schemas e Creando DataFrame
Pré-definir um schema para os dados vem com diversos benefícios:
- Você retira o onus do spark ter que inferir os tipos durante a leitura.
- Você impede o spark de criar uma job separada somente para ler os dados e criar o schema, o que para grandes datasets isso pode consumir muito tempo
- E você pode detectar erros previamente nos dados que não se enquadram no schema.

Temos duas formas de definir os tipos, uma programaticamente ou com Data Definition Language (DDL) stirng. Veja os exemplos das duas abaixo:

```py
# programaticamente
from pyspark.sql.types import *

schema = StructType([
    StructField("author", StringType(), nullable=False),
    StructField("title", StringType(), nullable=False),
    StructField("pages", IntegerType(), nullable=False),
])

# DDL

schema = "author STRING not null, title STRING not null, pages INT not null"
```

Veja abaixo como utilizar isso em um exemplo:

```py
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('myApp').getOrCreate()

data = [
    {'author': 'Stephen Hawking', 'title': 'Breves respostas para grandes questões', 'pages': 256},
    {'author': 'Carl Sagan', 'title': 'Cosmos', 'pages': 560},
    {'author': 'Yuval Noah Harari', 'title': 'Sapiens', 'pages': 433},
]

schema = "author STRING not null, title STRING not null, pages INT not null"

books = spark.createDataFrame(data, schema)

books.printSchema()
```

Agora, suponha que esses dados estavam em um csv

|author|title|pages|
|-|-|-|
|Stephen Hawking  |Breves respostas para grandes questões|256|
|Carl Sagan       |Cosmos                                |560|
|Yuval Noah Harari|Sapiens                               |433|

```py
from pyspark.sql import SparkSession
from pyspark.sql.types import *

spark = SparkSession.builder.appName('myApp').getOrCreate()

schema = StructType([
    StructField("author", StringType(), nullable=False),
    StructField("title", StringType(), nullable=False),
    StructField("pages", IntegerType(), nullable=False),
])

books = spark.read.schema(schema).csv('books.csv', header=True)

books.printSchema()
```

### Colunas e expressões

No spark, as colunas do dataframe são objetos com métodos publicos, você também pode utilizar expressões logicas ou matemáticas. Como por exemplo `expr("column_name * 5")` ou `expr("column_name > 5")`

> OBS.: expr está dentro de pyspark.sql.functions

Vamos ao exemplo:

```py
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder.appName('myApp').getOrCreate()

# criando o dataframe
data = [
    {'author': 'Stephen Hawking', 'title': 'Breves respostas para grandes questões', 'pages': 256},
    {'author': 'Carl Sagan', 'title': 'Cosmos', 'pages': 560},
    {'author': 'Yuval Noah Harari', 'title': 'Sapiens', 'pages': 433},
]

schema = "author STRING, title STRING, pages INT"

books = spark.createDataFrame(data, schema)
books.show()

# selecionando apenas a coluna de author
author_df = books.select('author')
author_df.show()

# selecionando a coluna de pages multiplicada por dois
pages_duble = books.select(expr('pages * 2'))
pages_duble.show()

# multiplicar pages por pages
books.select(expr('pages * pages')).show()

# adicionando uma coluna
time_to_read_a_page = (1/24/60)*3 # 3 min por página
books.withColumn('days_to_read', expr(f"pages * {time_to_read_a_page}")).show()
```

### Rows
Uma linha no Spark é um objeto genérico, que contém uma ou mais colunas. Como uma Row é um objeto e uma coleção ordenada de campos, você pode instanciar uma Row e acessar os campos por índices começando em 0.

```py
from pyspark.sql import Row

new_book_row = Row('Jules S Damji', 'Learning Spark: Lightning-Fast Data Analytics', 400)
new_author = new_book_row[0]
```

### Operações comuns com dataframes

Vamos ler o arquivo CSV com a base de ligações de incendio de são francisco, o dataset será o [sf-fire-calls](./data/sf-fire-calls.csv), porém precisamos definir o schema antes, pois esse arquivo possui 4.380.660 de registros, então se deixarmos o spark inferir os tipos, vamos demorar muito.

```py
from pyspark.sql.session import SparkSession
from pyspark.sql.types import *

spark = SparkSession.builder.appName('sfFire').getOrCreate()

## definindo schema
fire_schema = StructType(
    [
        StructField('CallNumber', IntegerType(), True),
        StructField('UnitID', StringType(), True),
        StructField('IncidentNumber', IntegerType(), True),
        StructField('CallType', StringType(), True),
        StructField('CallDate', StringType(), True),
        StructField('WatchDate', StringType(), True),
        StructField('CallFinalDisposition', StringType(), True),
        StructField('AvailableDtTm', StringType(), True),
        StructField('Address', StringType(), True),
        StructField('City', StringType(), True),
        StructField('Zipcode', IntegerType(), True),
        StructField('Battalion', StringType(), True),
        StructField('StationArea', StringType(), True),
        StructField('Box', StringType(), True),
        StructField('OriginalPriority', StringType(), True),
        StructField('Priority', StringType(), True),
        StructField('FinalPriority', IntegerType(), True),
        StructField('ALSUnit', BooleanType(), True),
        StructField('CallTypeGroup', StringType(), True),
        StructField('NumAlarms', IntegerType(), True),
        StructField('UnitType', StringType(), True),
        StructField('UnitSequenceInCallDispatch', IntegerType(), True),
        StructField('FirePreventionDistrict', StringType(), True),
        StructField('SupervisorDistrict', StringType(), True),
        StructField('Neighborhood', StringType(), True),
        StructField('Location', StringType(), True),
        StructField('RowID', StringType(), True),
        StructField('Delay', FloatType(), True),
    ]
)

# lendo o arquivo csv
sf_fire_file = './data/sf-fire-calls.csv'
fire_df = spark.read.csv(sf_fire_file, header=True, schema=fire_schema)
```

Agora vamos salvar o dataframe em Parquet, pois é um tipo de arquivo muito mais otimizado, com compressão e que mantem os tipos de dados como parte do seu metadata, ou seja, se formos ler um arquivo parquet, não precisamos definir o schema, pois ele já faz parte do arquivo.

```py
# salvando o dataframe em parquet
parquet_file_name = './data/sf-fire-calls.parquet'
fire_df.write.format('parquet').save(parquet_file_name)
```


> OBS.: Veja que o spark cria uma pasta com vários arquivos, isso acontece pois o spark salva os arquivos de forma particionada, pois ele joga cada arquivo em um worker diferente, isso facilita a paralelização. E a leitura você faz normalmente,  ou seja: `fire_df = spark.read.parquet('./data/sf-fire-calls.parquet')`

#### Transformations and actions

- Selecionar colunas
Para selecionar colunas específicas, basta utilizar o método de select dos dataframes

```py
fire_df.select("IncidentNumber", "AvailableDtTm", "CallType")
```

- Filter
Para filtrar os dados podemos utilizar dos metodos filter ou where

```py
from pyspark.sql.functions import *

fire_df.select('IncidentNumber', 'AvailableDtTm', 'CallType').where(
    col('CallType') != 'Medical Incident'
).show(5, truncate=False)
```

- Distinct
Vamos contar quantos tipos de ligações distintas existem dentro do dataset, para isso temos duas formas:

```py
## retorna um número com a quantidade
df_distinct_call_types = fire_df.select('CallType').where(
    col('CallType').isNotNull()
).distinct()
df_distinct_call_types.count()

## retorna um dataframe que possúi a coluna DistinctCallTypes com a quantidade
fire_df.select('CallType').where(col('CallType').isNotNull()).agg(
    countDistinct('CallType').alias('DistinctCallTypes')
).show()
```

- Rename columns

```py
new_fire_df = fire_df.withColumnRenamed('Delay', 'ResponseDelayedinMins')
new_fire_df.select('ResponseDelayedinMins').where(
    col('ResponseDelayedinMins') > 5
).show(5, False)
```

- Add column

```py
fire_ts_df = new_fire_df.withColumn('IncidentDate', to_timestamp(col('CallDate'), "MM/dd/yyyy"))
fire_ts_df.show(5, False)
```

- Drop Column

```py
fire_ts_df = fire_ts_df.drop('CallDate')
fire_ts_df.show(5, False)
```

- Funções de data
Agora que temos a coluna `IncidentDate` como o tipo timestamp, podemos utilizar as funções de year, month e day para extrair as informações

```py
fire_ts_df.select(
    year('IncidentDate').alias('Year'),
    month('IncidentDate').alias('Month'),
    dayofmonth('IncidentDate').alias('Day_Of_Month'),
    dayofweek('IncidentDate').alias('Day_Of_Week'),
    dayofyear('IncidentDate').alias('Day_Of_Year'),
).show(5, False)
```

- groupBy e order by
```py
fire_ts_df.select('CallType').where(col('CallType').isNotNull()).groupBy(
    'CallType'
).count().orderBy('count', ascending=False).show(10, truncate=False)
```

- sum, min, max, avg
```py
fire_ts_df.select(
    sum("NumAlarms").alias('sum_NumAlarms'), 
    avg("ResponseDelayedinMins").alias('avg_ResponseDelayedinMins'),
    min("ResponseDelayedinMins").alias('min_ResponseDelayedinMins'),
    max("ResponseDelayedinMins").alias('max_ResponseDelayedinMins')
).show()
```

# Spark SQL

Com o spark é possível efetuar operações SQL (compativeis com ANSI-2003-compliant) diretamente em arquivos (CSV, Parquet, Avro, JSON...) ou sistemas RDBMS (postgreSQL, MySQL) utilizando conectores JDBC/ODBC.

Para executar as queries SQL você pode utilizar o método sql() da SparkSession, como por exemplo:

```py
spark.sql("SELECT * FROM my_table_name")
```

e o retorno de toda query é sempre um DataFrame, que pode ser utilizado posteriomente.

vamos fazer alguns exemplos para entender como isso funciona.

- exemplo 01
vamos pegar os dados do dataset [departuredelays](./data/departuredelays.csv) e vamos executar as queries utilizando sql. Para isso, primeiro vamos utilizar o spark para ler o csv e retornar um DataFrame, e então vamos transformar esse dataframe em uma View Temporária.

```py
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder.appName('myApp').getOrCreate()

# lendo o csv
csv_path = './data/departuredelays.csv'
df = spark.read.csv(csv_path, header=True)

# criando a tabela temporária
df.createTempView('us_delay_flights_tbl')

# Fazendo um select
spark.sql(
    """
    SELECT 
        distance, origin, destination 
    FROM us_delay_flights_tbl WHERE distance > 1000 
    ORDER BY distance DESC
"""
).show(10)

## veja isso escrito com DataFrame API
df.select('distance', 'origin', 'destination').where(
    expr('distance > 1000')
).orderBy('distance', ascending=False).show(10)

# fazendo um select com case when
spark.sql(
    """
    SELECT delay, origin, destination,
    CASE
        WHEN delay > 360 THEN 'Very Long Delays'
        WHEN delay > 120 AND delay < 360 THEN 'Long Delays'
        WHEN delay > 60 AND delay < 120 THEN 'Short Delays'
        WHEN delay > 0 and delay < 60  THEN  'Tolerable Delays'
        WHEN delay = 0 THEN 'No Delays'
        ELSE 'Early'
    END AS Flight_Delays
    FROM us_delay_flights_tbl
    ORDER BY origin, delay DESC
"""
).show(10)
```

## SQL Tables and Views
Associado com cada tabela no Spark temos os metadados relevantes, como o schema, descrição dos dados, da tabela, como o nome da tabela, nome do banco de dados, nome das colunas, partições, localização no física dos dados.

E ao em vez de ter todo um metastore separado para o spark, o spark utiliza o Apache Hive metastore, assim simplificando o processo.

### Managed vs UnmanagedTables
Existem dois tipos de tabelas no spark: managed e unmanaged. Como o nome mesmo diz, as tabelas do tipo managed são gerenciadas pelo spark, dados e metadados. Esse gerenciamento pode acontecer utilizando-se o filesystem, hdfs, S3. Já a unmanaged é gerenciada por você usuário em um datasource externo, como o cassandra.

### Criando tabelas e databases SQL
vou utilizar como fonte de informação o dataset [departuredelays](./data/departuredelays.csv)

Para criar o banco de dados

```py
spark.sql("CREATE DATABASE learn_spark_db")
spark.sql("USE learn_spark_db")
```

> OBS.: Se você rodou isso (possuindo uma sparkSession), você deve ter notado que foi criada uma pasta chamada spark-werehouse e dentro delta uma outra chamada learn_spark_db.db que é onome do nosso banco de dados

#### Criar Manage Table
você pode fazer isso de duas formas, com sql ou com DataFrame API, vou mostrar ambas, mas vou rodar somente a do DataFrame API

- SQL:
```py
spark.sql("""
    CREATE TABLE managed_us_delay_flights_tbl (
        date        STRING, 
        delay       INT,  
        distance    INT, 
        origin      STRING, 
        destination STRING
    )
""")
```

- DataFrame API

```py
csv_path = './data/departuredelays.csv'
schema="date STRING, delay INT, distance INT, origin STRING, destination STRING"
df = spark.read.csv(csv_path, header=True, schema=schema)

df.write.saveAsTable("managed_us_delay_flights_tbl")
```

> OBS.: veja que agora apareceu um diretório cheio de arquivos chamado managed_us_delay_flights_tbl, ai é onde está sua tabela com os dados do csv

#### Criar Unmanage Table

```py
df.write.option(
    "path", "./spark-warehouse/learn_spark_db.db/us_flights_delay"
).saveAsTable("us_delay_flights_tbl")
```

### Criando Views
No spark, podemos criar dois tipos de views, as com scopo global (visivel para todas as sessões) ou de sessão (visivel apenas para a sessão que a criou). Sendo que uma view é sempre temporária, ou seja, elas desaparecem depois que a aplicação spark terminar.

Uma vez que tenha criado uma view, você pode executar queries nela, assim como em uma tabela. A diferença é que a view não possui os dados, e que a tabela persiste depois do termino da aplicação spark.

#### Criando view global

```py
# selecionar apenas parte de uma tabela
df_sfo = spark.sql("""
    SELECT 
        date, delay, origin, destination 
    FROM managed_us_delay_flights_tbl
    where origin = 'SFO'
""")

# criar a view com o resultado do select
df_sfo.createOrReplaceGlobalTempView("us_origin_airport_SFO_global_tmp_view")
```

Agora para acessar algo de uma view global você deve utilizar sempre `global_temp` na frente do nome da view `global_temp.<view>` porque o spark cria a view em um diretório global temporário chamado `global_temp`

```py
spark.sql("select * from global_temp.us_origin_airport_SFO_global_tmp_view").show() 
```

#### Criando view de sessão

```py
# selecionar apenas parte de uma tabela
df_jfk = spark.sql("""
    SELECT 
        date, delay, origin, destination 
    FROM managed_us_delay_flights_tbl
    where origin = 'JFK'
""")

# criar a view com o resultado do select
df_jfk.createOrReplaceTempView("us_origin_airport_JFK_tmp_view")
```

Agora para acessar, basta chamar pela view, ou seja, não precisa do `global_temp`

```py
spark.sql("select * from us_origin_airport_JFK_tmp_view").show()
```

#### Drop view

Se quiser dropar as views, é bem simpes

```py
# para a global
spark.catalog.dropGlobalTempView("us_origin_airport_SFO_global_tmp_view")

# para a de sessão
spark.catalog.dropTempView("us_origin_airport_JFK_tmp_view")
```

# Conexão com Banco de Dados
Conexões com banco de dados são feitas utilizando JDBC, cada banco tem o seu driver JDBC é só uma questão de pesquisar. Aqui eu utilizarei o postgresql para exemplificar.

- criando docker
```yml
version: "3"

services:
    postgres:
        image: postgres 
        environment: 
            POSTGRES_PASSWORD: 123
            POSTGRES_USER: postgres
            POSTGRES_DB: app_db
        ports: 
            - "5432:5432"
        volumes: 
            - ~/.pgdata:/var/lib/postgresql/data
```

- criando tabela
```sql
create table tb_user (
    name text,
    age int
)
```

- inserindo dados
```sql
insert into tb_user (name, age) values ('lucas', 27)
```

- Baixe o driver do postgres pelo [maven](https://mvnrepository.com/artifact/org.postgresql/postgresql), eu vou utilizar aqui a versão 42.3.3 que é a mais atual nesse momento.

- Agora copie o `.jar` para dentro da pasta de jars do pyspark. Aqui no meu caso, eu estou com um venv, logo o caminho é `.venv/lib/python3.10/site-packages/pyspark/jars`

- Executando comandos
```py
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder.appName('myApp').getOrCreate()

# conexão de leitura
tb_user = (
    spark.read.format('jdbc')
    .option('url', 'jdbc:postgresql://localhost:5432/app_db')
    .option('dbtable', 'tb_user')
    .option('user', 'postgres')
    .option('password', '123')
    .load()
)

# fazendo select
tb_user.select('name').where(col('age') > 4).show()

# criando novos dados para fazer insert
new_data = spark.createDataFrame(
    [
        {'name': 'joão', 'age': 26},
        {'name': 'tadeu', 'age': 44},
        {'name': 'xavis', 'age': 50},
    ]
)

# inserindo os dados
new_data.write.format('jdbc').mode('append').option(
    'url', 'jdbc:postgresql://localhost:5432/app_db'
).option('dbtable', 'tb_user').option('user', 'postgres').option(
    'password', '123'
).save()
```

> OBS.: Cada banco tem um jeito diferente de conectar, mas é só dar uma pesquisada que vai conseguir encontrar como é. No livro [Learning Spark: Lightning-Fast Data Analytics](https://www.amazon.com.br/Learning-Spark-2e-Jules-Damji/dp/1492050040/ref=sr_1_7?__mk_pt_BR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=25980MX842H5S&keywords=learning+pyspark&qid=1645971242&sprefix=learning+pyspark%2Caps%2C175&sr=8-7&ufe=app_do%3Aamzn1.fos.25548f35-0de7-44b3-b28e-0f56f3f96147) no capitulo 5 tem uma sessão chamada External Data Sources, que explica como conectar ao PostgreSQL, mySQL, Azure Cosmos DB e MS SQL Server.

### conexão para oracle
Já adianto que não consegui fazer  essa conexão funcionar tão bem, porém irei dizer o que fiz, assim pode ajudar alguém (ou até a mim mesmo no futuro) a começar a arrumar isso (e vai que em um banco não xe isso funciona normalmente)

- começei com o container do xe (esse eu mesmo montei, está no meu dockerhub)
```yml
version: "3"

    oracle:
        image: lucasfdutra/oracle-xe
        ports:
            - "1521:1521"
        volumes:
            - "./data:/shared"
        environment:
            - ORACLE_PWD=123
```

- paramâmetros de conexão
    - host: localhost
    - port: 1521
    - database: XEPDB1
    - user: system
    - pass: 123

- Criando tabela
```sql
create table tb_user (
    name varchar(200),
    age integer
)
```

- Inserindo uma linha 
```sql
insert into tb_user (name, age) values ('lucas', 27)
```

- Baixar o driver no [maven](https://mvnrepository.com/artifact/com.oracle.database.jdbc/ojdbc8/21.5.0.0). Tive que usar essa versão em específica, pois é a que funciona com a versão do meu java, mas caso de erro de versão, você vai tentando ai até dar certo com a sua.

- O código para leitura e escrita fica como mostrado abaixo. Porém ao ler a tabela a linha que inseri antes não é retornada, porém após inserir os novos dados utilizando o spark e efetuando a leitura novamente, os novos resultados vieram.

```py
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder.appName('myApp').getOrCreate()

# conexão de leitura
tb_user = (
    spark.read.format('jdbc')
    .option('driver', 'oracle.jdbc.driver.OracleDriver')
    .option('url', 'jdbc:oracle:thin:@localhost:1521/XEPDB1')
    .option('dbtable', 'tb_user')
    .option('user', 'system')
    .option('password', '123')
    .load()
)

# fazendo select
tb_user.show()

""" resposta (vazia)
+-----+---+                                                                     
| NAME|AGE|
+-----+---+
|     |   |
+-----+---+
"""

# criando novos dados para fazer insert
new_data = spark.createDataFrame(
    [
        {'name': 'joão', 'age': 26},
        {'name': 'tadeu', 'age': 44},
        {'name': 'xavis', 'age': 50},
    ]
)

# inserindo os dados
(
    new_data.write.format('jdbc')
    .mode('append')
    .option('url', 'jdbc:oracle:thin:@localhost:1521/XEPDB1')
    .option('driver', 'oracle.jdbc.driver.OracleDriver')
    .option('dbtable', 'tb_user')
    .option('user', 'system')
    .option('password', '123')
    .save()
)

# Lendo novamente
tb_user = (
    spark.read.format('jdbc')
    .option('driver', 'oracle.jdbc.driver.OracleDriver')
    .option('url', 'jdbc:oracle:thin:@localhost:1521/XEPDB1')
    .option('dbtable', 'tb_user')
    .option('user', 'system')
    .option('password', '123')
    .load()
)

tb_user.show()

""" resposta retorna somente o que o spark inseriu
+-----+---+                                                                     
| NAME|AGE|
+-----+---+
| joão| 26|
|xavis| 50|
|tadeu| 44|
+-----+---+
"""
```

# Contrução de um Data Lake

A grosso modo, data lakes são locais para se armazenar muitos arquivos, mas vamos explanar melhor essa ideia vendo primeiramente algumas propriedades desejadas para soluções de armazenamento

- Scalabilidade e performance
    - A solução de armazenamento deve escalar para comportar o volume de dados e também prover read/write throuphput e baixa latências para os serviços que o consomem

- Transaction Support
    - Normalmente os serviços irão ler e escrever dados simultâneamente em paralelo, logo suporte a ACID transactions é indispensável.

- Suporte para diversos formatos de dados
    - Deve suportar diversos tipos de dados, como arquivos de texto (logs), arquivos semi-estruturados (json) e estruturados (dados tabulares - parquet)

- Suportar diversos workloads
    - Deve conseguir se comunicar (ser consumida) com uma variedade de serviços, como por exemplo:
        - BI
        - Batch workloads (ETL jobs)
        - Straming
        - ML e AI

- Opennes
    - Para facilitar a integração com diversos sistemas o ideal é armazenar os dados em formatos abertos

## Databases
Quando se fala de soluções de armazenamento, provavelmente a primeira coisa que vem na cabeça são os bancos de dados (PostgreSQL, mysql). E eles são otimos mesmo, porém nem sempre eles são a melhor solução.

Veja bem, SQL workloads em databases podem ser classificados de duas formas:
- Online Transaction Processing (OLTP)
    - Operações altamente concorrentes, que exigem baixa latência, com queries simples que normalmente efetuam leitura ou update em poucos registros de uma vez
- Online Analyical Processing (OLAP)
    - Tipicamente com queries complexas (varias agregações e joins) e que necessitam de high-throuphput scans em cima de muitos registros

O apache spark é uma ferramenta desenhada primáriamente para OLAP workloads e não OLTP

E quanto aos bancos de dados tradicionais, aqui vão algumas limitações:

### Limitações de Bancos de Dados

- Crescimento do tamanho dos dados
    - A quantidade de dados coletados hoje aumentou muito, nas últimas decadas nos fomos de gigabytes para terabytes e agora petabytes.

- Aumento da complexidade das analises
    - Com a disponibilidade de mais dados, as analises desses dados ficaram cada vez mais complexas, como por exemplo machine learning e deep learning

- Bancos de dados são extremamente caros de escalar horizontalmente (scale out)
    - Bancos de dados são extremamente eficientes em processar dados em uma única máquina. Porém hoje o unico jeito de processar tantos dados é com escalabilidade horizontal, ou seja, processamentos paralelos.Porém bancos dados, especialmente os open sources, não são desenhados para isso, e alguns poucos que conseguem são proprietários e extremamente caros.

- Bancos de dados não suportam muito bem as analises de dados non-SQL
    - Normalmente os bancos de dados armazenam os dados de forma otimizada para operações SQL. Ou seja, qualquer outra ferramenta de processamente, como as de machine learning e deep learning não conseguem acessar os dados de forma eficiente.

## Data Lakes
Agora sim, vamos para os data lakes.

A arquitetura de Data lake, diferentemente de databases, desacoplam os sistemas de storage com o de processamento. Isso permite que ambos os sistemas escalam independentemente um do outro de acordo com a necessidade. E além de tudo, os dados são salvos em formatos abertos de arquivos facilitando a integração entre o storage e qualquer workload que queira.

Normalmente a construção de data lakes se da na escolha das seguintes ferramentas:

- Storage system
    - Pode ser HDFS em um cluster de máquinas (hadoop) ou qualquer object storage (AWS S3, Azure Data Lake Storage ou Google Cloud Storage)

- File format
    - Dependendo da demanda, os dados podem ser salvos em arquivos de formato estruturado (parquet, ORC) ou semi-estruturado (json) ou em alguns casos até mesmo dados desestruturados (images, textos, audios, videos)

- Processing engine(s)
    - Tudo depende do que será processado e para o que. Como por exemplo processamento de dados em batch (spark, presto, apache hive), stream de dados (Spark, apache flink) ou machine learning (spark mllib, scikit-learn, R)

Essa flexibilidade na escolha do sistema de armazenamento, formato dos arquivos e engine de processamento já são por si só uma grande vantagem com relação aos databases, porém, um data lake oferece a mesma performance por um preço muito mais barato.

> OBS.: Para se conectar com o hadoop filesystem não é necessário fazer nada, já para se conectar com o S3 é preciso realizar algumas configurações, mas mostrarei como fazer isso depois.

Data lakes parecem ser maravilhosos não é? mas eles tem uma grande falha, eles não provêm ACID transactions, e se você se lembra das propriedades desejadas para uma soluções de armazenamento essa era indispensável.

## Conectar ao S3
Primeiramente, veja qual versão é utilizada para os seus pacotes .jar do hadoop, no meu caso, como estou utilizando o pyspark via pip, eles estão dentro de `.venv/lib/python3.10/site-packages/pyspark/jars`, então basta eu rodar o seguinte comando:

```sh
ls .venv/lib/python3.10/site-packages/pyspark/jars | grep hadoop
```

e a resposta foi:

```sh
dropwizard-metrics-hadoop-metrics2-reporter-0.1.2.jar
hadoop-client-api-3.3.1.jar
hadoop-client-runtime-3.3.1.jar
hadoop-shaded-guava-1.1.1.jar
hadoop-yarn-server-web-proxy-3.3.1.jar
parquet-hadoop-1.12.2.jar
```

Pelo pacote `hadoop-client-api` posso ver que a versão é a `3.3.1`, agora eu preciso buscar no [mvnrepository](https://mvnrepository.com/artifact/org.apache.hadoop/hadoop-aws) o pacote `hadoop-aws` na versão `3.3.1` (se for versão diferente vai dar erro) e baixar o seu jar. Também será necessário baixar o jar da [sdk da aws](https://mvnrepository.com/artifact/com.amazonaws/aws-java-sdk-bundle), esse não importa a versão, pode baixar a mais recente (peguei a 1.12.189).

No caso, eu posso só rodar os comandos abaixo com wget e então mandar os arquivos para dentro da pasta com meus jars

```sh
wget -P .venv/lib/python3.10/site-packages/pyspark/jars/ https://repo1.maven.org/maven2/org/apache/hadoop/hadoop-aws/3.3.1/hadoop-aws-3.3.1.jar

wget -P .venv/lib/python3.10/site-packages/pyspark/jars/ https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk-bundle/1.12.189/aws-java-sdk-bundle-1.12.189.jar
```


> OBS.: se não quiser baixar esse bundle da aws sdk, pode baixar separadamente os seguinte pacotes: `aws-java-sdk-1.12.189`, `aws-java-sdk-core-1.12.189`, `aws-java-sdk-dynamodb-1.12.189`, `aws-java-sdk-s3-1.12.189` (todos na mesma versão), pelo que eu testei isso já é o suficiente.

Agora que você já tem os pacotes, você precisa fazer algumas configurações no spark para que ele leia do s3

> OBS.: Presumo que tenha um bucket na aws e um usuário com credenciais para acessá-lo, bem como que essas credenciais estão nas suas variáveis de ambiente de alguma maneira

```py
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
from os import getenv

AWS_ACCESS_KEY_ID = getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = getenv('AWS_SECRET_ACCESS_KEY')

sc = SparkContext()

sc._jsc.hadoopConfiguration().set('com.amazonaws.services.s3.enableV4', 'true')
sc._jsc.hadoopConfiguration().set('fs.s3a.impl', 'org.apache.hadoop.fs.s3a.S3AFileSystem')
sc._jsc.hadoopConfiguration().set('fs.AbstractFileSystem.s3a.imp', 'org.apache.hadoop.fs.s3a.S3A')

sc._jsc.hadoopConfiguration().set('fs.s3a.access.key', AWS_ACCESS_KEY_ID)
sc._jsc.hadoopConfiguration().set('fs.s3a.secret.key', AWS_SECRET_ACCESS_KEY)

spark = SparkSession(sc)

# lendo um csv de dentro do s3
df = spark.read.csv('s3a://mybucket/books.csv')

# salvando em formato parquet
df.write.parquet('s3a://mybucket/books.parquet')
```

#### localstack
O localstack é uma ferramenta que simula a aws em ambiente local no docker, você pode ver mais sobre ela no [repositório da ferramenta](https://github.com/localstack/localstack). E caso queira utilizar o s3 localmente com o auxilio dessa ferramenta, basta utilizar os mesmos pacotes .jar, porém com duas pequenas diferenças nas configurações do código python

```py
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession

AWS_ACCESS_KEY_ID = 123 # o localstack não precisa de credenciais validas
AWS_SECRET_ACCESS_KEY = 123 # o localstack não precisa de credenciais validas

sc = SparkContext()

sc._jsc.hadoopConfiguration().set('com.amazonaws.services.s3.enableV4', 'true')
sc._jsc.hadoopConfiguration().set('fs.s3a.impl', 'org.apache.hadoop.fs.s3a.S3AFileSystem')
sc._jsc.hadoopConfiguration().set('fs.AbstractFileSystem.s3a.imp', 'org.apache.hadoop.fs.s3a.S3A')
sc._jsc.hadoopConfiguration().set("fs.s3a.aws.credentials.provider", 'com.amazonaws.auth.InstanceProfileCredentialsProvider,com.amazonaws.auth.DefaultAWSCredentialsProviderChain')

sc._jsc.hadoopConfiguration().set('fs.s3a.access.key', AWS_ACCESS_KEY_ID)
sc._jsc.hadoopConfiguration().set('fs.s3a.secret.key', AWS_SECRET_ACCESS_KEY)
sc._jsc.hadoopConfiguration().set('fs.s3a.endpoint', 'http://0.0.0.0:4566') # usar localhost não deu muito certo

spark = SparkSession(sc)

# lendo um csv de dentro do s3
df = spark.read.csv('s3a://mybucket/books.csv')

# salvando em formato parquet
df.write.parquet('s3a://mybucket/books.parquet')
```

Caso queira, esse é o docker-compose.yml que utilizei para criar o localstack:

```yml
version: "3"

services:
    localstack:
        image: localstack/localstack
        ports:
            - "4566:4566"
        environment:
            - SERVICES=s3,lambda,cloudformation,cloudwatch,sts,iam,dynamodb
            - LAMBDA_EXECUTOR=docker
            - DOCKER_HOST=unix:///var/run/docker.sock
        volumes:
            - "/var/run/docker.sock:/var/run/docker.sock"
```

# Lakehouses
Lakehouses são um novo paradigma que combina os melhores elementos de datalakes com de warehouses (que são databases). Essa combinação nos da um sistema similar a um banco de dados, mas de baixo custo e escalavel. Segue algumas características:

- Suporte a ACID transactions
- Schema enforcement
    - Previne a entrada de dados com schema incorreto.
    - O schema pode evoluir para acomodar mudanças de dados (Alter table)
- Suporte a diversos formatos abertos de armazenamento de dados
- Suporte a diversos tipos de workloads
- Suporte a upserts e deletes
- Data Governance
    - Possuem ferramentas que possibilitam a verificação de mudanças nos dados e sua integridade.

Existem algumas ferramentas open source para montar um lakehouse, são elas: `Apache Hudi`, `Apache Iceberg` e `Delta Lake`. Todos os três possuem as seguintes características em comum:

- Armazenam grandes volumes de dados em arquivos de formato estruturado em filesystems escaláveis
- Mantem uma linha do tempo de logs de modificações atômicas
- Utiliza os logs para definir versões das tabelas e provêm snapshots isolados entre leituras e escritas
- Suportam leitura e escrita nas tabelas usando o apache Spark.

Cada um desses projetos possuem características unicas em termos de APIs, performance e level de integração com o spark. Porém todas elas estão evoluindo muito e muito rápido.

-  [Apache Hudi](https://hudi.apache.org/)
Desenvolvido pela Uber, o apachi hudi foi feito para upserts incrementais e deletes para dados no estilo key/value. Os dados são armazenados de forma a combinar formatos colunares (parquet) e row-based (arquivos avro para guardar informações sobre as modificações feitas nos arquivos parquet)

- [Apache Iceberg](https://iceberg.apache.org/)
Desenvolvido pela Netflix, o apache iceberg, diferentemente do hudi que foca em upserts com key/value, é focado mais em proposito geral para dados que escalam para petabytes em uma única tabela e que possuem prpriedades de schema evolution (adicionar, deletar, update, rename, reordenar colunas, campos e estruturas compostas) 

- [Delta Lake](https://delta.io/)
O Delta Lake é um projeto open source, hospedado pela Linux Foundation, que foi desenvolvido pelos mesmos criadores do Apache Spark. Assim como os demais ele possui update, delete, merge (upsert) com api para java, scala e python. Possui suporte a leitura e escrita de tabelas usando fontes de dados em straming. As operações de merge possui condições condições complexas assim como o proprio merge do SQL.


## Conectar a cada um deles

> OBS.: Caso não queira trabalhar com o S3 nos seus testes com essas ferramentas, utilize seu proprio localstorage, será mais simples. Para isso basta referenciar os caminhos com `file://` e passar o caminho completo para onde deseja colocar as tabelas, como por exemplo: `file:///home/userx/testes/hudi_table`.

### Hudi

```py
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
from pyspark.conf import SparkConf

conf = SparkConf()

# puxando pacote do hudi
conf.set('spark.jars.packages', 'org.apache.spark:spark-avro_2.12:3.1.2,org.apache.hudi:hudi-spark3.1.2-bundle_2.12:0.10.1')
conf.set('spark.serializer', 'org.apache.spark.serializer.KryoSerializer')

sc = SparkContext(conf=conf)

#**** aqui vão as configurações do s3 caso queira ****#

spark = SparkSession(sc)
```

Se estiver trabalhando com o hudi, e acontecer de aparecer esse erro aqui:

```sh
Py4JJavaError: An error occurred while calling o64.load.
: java.lang.NoSuchMethodError: org.apache.spark.sql.catalyst.util.DateFormatter
```

é porque o hudi não tem suporte para a versão 3.2.x do spark (pelo menos não nesse momento 2022-04-15), então volte o seu spark para a versão 3.1.x e dai você vai conseguir utilizar ele, mas se lembre de refazer as configurações para conectar ao s3, pois agora a versão dos pacotes hadoop devem ter mudado.


### Delta

Ele não tem problemas de compatibilidade com o spark (afinal ambos são feitos pelo mesmo pessoal), mas versões do delta funcionam com versões específicas do spark, então veja a tabela de compatibilidade [aqui](https://docs.delta.io/latest/releases.html), no meu caso, eu vou estar com spark 3.2.1 e delta 1.2.0


```py
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
from pyspark.conf import SparkConf

conf = SparkConf()

# puxando pacote do delta
conf.set('spark.jars.packages', 'io.delta:delta-core_2.12:1.2.0')

# configurando o context para o delta
conf.set('spark.sql.extensions', 'io.delta.sql.DeltaSparkSessionExtension')
conf.set('spark.sql.catalog.spark_catalog', 'org.apache.spark.sql.delta.catalog.DeltaCatalog')

sc = SparkContext(conf=conf)

#**** aqui vão as configurações do s3 caso queira ****#

spark = SparkSession(sc)
```

### Iceberg

infelizmente eu não entendi tão bem como usar ele com o s3, mas tem lá na documentação. Porém vou colocar aqui um exemplo utilizando o localstorage mesmo

```py
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
from pyspark.conf import SparkConf
from os import getcwd

conf = SparkConf()

# configurações para o iceberg
conf.set('spark.jars.packages', 'org.apache.iceberg:iceberg-spark-runtime-3.2_2.12:0.13.1')

conf.set("spark.sql.extensions", "org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions")
conf.set("spark.sql.catalog.spark_catalog", "org.apache.iceberg.spark.SparkSessionCatalog")
conf.set("spark.sql.catalog.spark_catalog.type", "hive")
conf.set("spark.sql.catalog.local", "org.apache.iceberg.spark.SparkCatalog")
conf.set("spark.sql.catalog.local.type", "hadoop")
conf.set("spark.sql.catalog.local.warehouse", f"{getcwd()}/warehouse")

sc = SparkContext(conf=conf)
spark = SparkSession(sc)

# criando a tabela
spark.sql("CREATE TABLE local.db.table (id bigint, data string) USING iceberg")

# inserindo e puxando da tabela
spark.sql("INSERT INTO local.db.table VALUES (1, 'a'), (2, 'b'), (3, 'c')")
df = spark.sql("SELECT * FROM local.db.table")

# fazendo um merge
spark.sql("""
    MERGE INTO local.db.table t 
    USING (
        SELECT 1 as id, 'z' as data FROM local.db.table where id = 1
    ) u ON t.id = u.id
    WHEN MATCHED THEN 
        UPDATE SET t.data = u.data
    WHEN NOT MATCHED THEN 
        INSERT *
""")

## exibindo os resultados
spark.sql("SELECT * FROM local.db.table").show()
```
